create table persons(
    email text primary key,
    password text not null,
    salt text not null,
    first_name text,
    last_name text
);
