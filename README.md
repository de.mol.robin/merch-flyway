# Inetum merch shop db init scripts (flyway)

Assuming you have docker installed, run:

    docker run --detach --name pg -p 5432:5432 -v pgdata:/var/lib/postgresql/data -e POSTGRES_PASSWORD=acaddemicts -e POSTGRES_USER=acaddemicts postgres:alpine

This will create a container named "pg", running a postgresql database listening on port 5432, with database, username and password "acaddemicts".

Next, from the root of this project, run:

    mvn flyway:clean flyway:migrate

This will run the flyway migrations in this project (through the flyway maven plugin), which will create a bunch of tables and generate some dummy data.
