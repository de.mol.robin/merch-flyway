create type mug_size as enum ('small', 'medium',  'large', 'XL');

create table mugs(
    id bigint primary key references merch(id),
    mugsize mug_size not null
);
