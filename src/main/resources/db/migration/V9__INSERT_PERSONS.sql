insert into persons (email, password, first_name, last_name)
 values ('john.doe@realdolmen.com', 'abc123', 'John', 'Doe');

insert into persons (email, password, first_name, last_name)
 values ('jane.doe@realdolmen.com', '123abc', 'Jane', 'Doe');

insert into persons (email, password, first_name, last_name)
 values ('jim.doe@realdolmen.com', 'a1b2c3', 'Jim', 'Doe');
