insert into
  orders (
    customer_id,
    purchase_date,
    delivery_date,
    street,
    nr,
    extra,
    city,
    postal_code,
    country
  )
values
  (
    1,
    to_date('20210116', 'YYYYMMDD'),
    to_date('20210214', 'YYYYMMDD'),
    'Nieuwescheldestraat',
    9,
    null,
    'Zwijnaarde',
    9052,
    'BE'
  );
insert into
  orders (customer_id, purchase_date, delivery_date)
values
  (
    3,
    to_date('20200520', 'YYYYMMDD'),
    to_date('20210121', 'YYYYMMDD'),
    'Steenvoordelaan',
    53,
    'A',
    'Gentbrugge',
    9050,
    'BE'
  );
insert into
  orderlines (order_id, merch_id, amount)
values
  (1, 3, 1);
insert into
  orderlines (order_id, merch_id, amount)
values
  (1, 4, 2);
insert into
  orderlines (order_id, merch_id, amount)
values
  (2, 1, 3);
insert into
  orderlines (order_id, merch_id, amount)
values
  (2, 4, 2);
insert into
  orderlines (order_id, merch_id, amount)
values
  (2, 5, 1);
