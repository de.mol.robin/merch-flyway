create table merch(
    id bigserial primary key,
    name text not null,
    description text,
    price real not null
);
