create table orders(
    id bigserial primary key,
    customer_id bigint references persons(id) not null,
    street text,
    nr integer,
    extra text,
    city text,
    postal_code text,
    country text,
    purchase_date date not null,
    delivery_date date not null,
    paid boolean not null default false
);
