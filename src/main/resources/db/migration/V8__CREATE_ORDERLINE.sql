create table orderlines(
    order_id bigint not null references orders(id),
    merch_id bigint not null references merch(id),
    amount integer not null,
    primary key (order_id, merch_id)
);
