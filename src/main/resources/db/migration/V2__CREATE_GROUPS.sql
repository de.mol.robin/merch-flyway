create table groups(
    name text not null,
    person_id bigint not null references persons(id),
    primary key(name, person_id)
);
