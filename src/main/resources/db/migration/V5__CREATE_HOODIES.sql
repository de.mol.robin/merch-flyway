create table hoodies(
    id bigint primary key references merch(id),
    size text,
    color text
);
