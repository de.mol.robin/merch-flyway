insert into merch (name, description, price)
 values ('coachie', 'The hoodie for coaches', 19.99);
insert into merch (name, description, price)
 values ('unie', 'The hoodie for unit managers', 29.99);
insert into merch (name, description, price)
 values ('johnnie', 'The hoodie for the one and only CEO', 199.99);
insert into merch (name, description, price)
 values ('Java', 'The best mug to drink Java coffee', 9.99);
insert into merch (name, description, price)
 values ('DotNet', 'Almost good enough to hold Java coffee', 4.99);
